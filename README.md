# Minimalistic Weather App

![logo](/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png)

A minimalistic weather app for Android which provides information about the current weather conditions and forecasts for multiple locations. The necessary weather data is requested from the [openweathermap](http://openweathermap.org) API.

## Impressions

![main activity](/pictures/main_panel.png)

## Requirements
