package com.example.simon.weather.DAO;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.simon.weather.models.WeatherLocation;

/**
 * Created by Simon on 03.07.18.
 */

@Database(entities = {WeatherLocation.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract WeatherLocationDao weatherLocationDao();
}
