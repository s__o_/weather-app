package com.example.simon.weather.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.simon.weather.models.WeatherLocation;

import java.util.List;

/**
 * Created by Simon on 03.07.18.
 */

@Dao
public interface WeatherLocationDao {
    @Query("SELECT * FROM WeatherLocation")
    List<WeatherLocation> getAll();

    @Insert
    void insertAll(WeatherLocation ... locations);

    @Query("SELECT uid FROM WeatherLocation ORDER BY uid DESC LIMIT 1")
    int getHighestId();

    @Delete
    void delete(WeatherLocation location);
}
