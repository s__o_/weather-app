package com.example.simon.weather;

import android.app.DialogFragment;
import android.arch.persistence.room.Room;
import android.content.ClipData;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenuItemView;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.simon.weather.DAO.AppDatabase;
import com.example.simon.weather.models.WeatherLocation;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.acl.Group;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        WeatherController.AsyncResponse,
        NavigationView.OnNavigationItemSelectedListener,
        AddLocationDialog.AddLocationDialogListener {

    private AppDatabase db;
    private ArrayList<WeatherLocation> locations = new ArrayList<>();
    private int position = 0;
    private String city = "Berlin";
    private String country = "de";
    private DrawerLayout mDrawerLayout;
    private Menu menu;
    private WeatherController wc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "weatherDB").allowMainThreadQueries().build();

        WeatherLocation.COUNT = db.weatherLocationDao().getHighestId();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView mNavigationView = findViewById(R.id.nav_view);
//        mNavigationView.inflateMenu(R.menu.drawer_view);
        menu = mNavigationView.getMenu();
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }
//        this.wc = new WeatherController(this, this.city, this.country);
        if(locations.size() > 0) {
            update(locations.get(0).getUid());
        }

    }


    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_add) {
            DialogFragment addLocationDialog = new AddLocationDialog();
            addLocationDialog.show(getFragmentManager(), "new location");
        } else if (id == R.id.nav_current) {
            // TODO add current location behaviour
        } else {
            this.city = item.getTitle().toString();
            this.country = "de";
        }
        update(id);
//        this.wc = new WeatherController(this, this.city, this.country);
//        this.wc.execute();

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void processFinish(JSONObject current, JSONObject forecast) {
        try {

            JSONArray weatherForecast = new JSONArray(forecast.getJSONArray("list").toString());

            JSONObject weatherData = new JSONObject(current.getJSONArray("weather").get(0).toString());
            JSONObject sysData = new JSONObject(current.get("sys").toString());
            JSONObject mainData = new JSONObject(current.get("main").toString());
            JSONObject windData = new JSONObject(current.get("wind").toString());
            Log.v("MainActivity", "weather keys: " + weatherForecast);

            // set location name in ActionBar
            getSupportActionBar().setTitle(Html.fromHtml("<font color='#000000'><b>" + current
                    .getString("name") + "</b> | " + sysData.getString("country") + "" +
                    "</font>"));

            // sunrise
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            TextView sunrise = findViewById(R.id.sunrise);
            Date upTime = new Date(sysData.getLong("sunrise") * 1000);
            sunrise.setText(timeFormat.format(upTime));

            // sunset
            TextView sunset = findViewById(R.id.sunset);
            Date downTime = new Date(sysData.getLong("sunset") * 1000);
            sunset.setText(timeFormat.format(downTime));

            // sunprogress
            ProgressBar progressBar = findViewById(R.id.progress);
            progressBar.setProgress(this.wc.dayProgress(upTime, downTime));

            // chart
            CombinedChart chart = findViewById(R.id.chart);
            chart.setData(wc.drawChart(this, weatherForecast, chart));
            chart.invalidate();

            // forecast icons
            for (int i = 1; i <= 9; i++) {
                JSONObject a = new JSONObject(weatherForecast.get(i).toString());
                JSONObject weather = new JSONObject(a.getJSONArray("weather").get(0).toString());
                int viewId = 0;
                switch (i) {
                    case 1:
                        viewId = R.id.forecast1;
                        break;
                    case 2:
                        viewId = R.id.forecast2;
                        break;
                    case 3:
                        viewId = R.id.forecast3;
                        break;
                    case 4:
                        viewId = R.id.forecast4;
                        break;
                    case 5:
                        viewId = R.id.forecast5;
                        break;
                    case 6:
                        viewId = R.id.forecast6;
                        break;
                    case 7:
                        viewId = R.id.forecast7;
                        break;
                    case 8:
                        viewId = R.id.forecast8;
                        break;
                    case 9:
                        viewId = R.id.forecast9;
                        break;
                }
                ImageView imageV = findViewById(viewId);
                imageV.setImageResource(wc.getWeatherIcon(weather.getInt("id")));
            }

            // weather icon
//            int weatherCode = weatherData.getInt("id");
            int iconId = wc.getWeatherIcon(weatherData.getInt("id"));
//            Log.v("MainActivity", "iconId: "+ weatherCode);
            // weather icon
            ImageView img = findViewById(R.id.weather_icon);
            img.setImageResource(iconId);

            // weather desc
            TextView desc = findViewById(R.id.weather_desc);
            desc.setText(wc.getWeatherDesc(weatherData.getInt("id")));

            // temperature
            TextView temp = findViewById(R.id.weather_temp);
            temp.setText(String.format("%.1f", mainData.getDouble("temp")) + "°");

            // max temp
//            TextView max_temp = findViewById(R.id.weather_max_temp);
//            max_temp.setText(mainData.getInt("temp_max") + "°");

            // min temp
//            TextView min_temp = findViewById(R.id.weather_min_temp);
//            min_temp.setText(mainData.getInt("temp_min") + "°");

            // wind
            TextView wind = findViewById(R.id.wind);
            wind.setText(String.format("%.1f", windData.getDouble("speed")));

            // pressure
            TextView pressure = findViewById(R.id.pressure);
            pressure.setText(mainData.getInt("pressure") + "");

            // humidity
            TextView humidity = findViewById(R.id.humidity);
            humidity.setText(mainData.getInt("humidity") + "%");


            ImageView loadingAnimation = findViewById(R.id.loading_image);
            ((AnimationDrawable) loadingAnimation.getDrawable()).stop();
            loadingAnimation.setVisibility(View.INVISIBLE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
//        MenuInflater menuInflater = getMenuInflater();


        EditText input = dialog.getDialog().findViewById(R.id.new_location_input);
        WeatherLocation newLocation = new WeatherLocation(input.getText().toString().trim(), "de");
        this.locations.add(newLocation);
        this.addMenuItem(newLocation);

        this.city = newLocation.getCity();
        db.weatherLocationDao().insertAll(newLocation);

//        wc = new WeatherController(this, this.city, this.country);
//        wc.execute();
        int s = menu.size();
        position =locations.size() - 1;
        update(newLocation.getUid());
        Log.v("MainActivity", "new location " + newLocation.getCity());
    }

    private void changeCity() {

    }

    private void addMenuItem(final WeatherLocation location) {
        menu.add(menu.getItem(0).getGroupId(), location.getUid(), 1,
                location.getCity());

        final MenuItem item = menu.getItem(menu.size() - 2);
        item.setCheckable(true);
        item.setChecked(true);

        // add delete button
        item.setActionView(new ImageView(this));
        ImageView tv = (ImageView) item.getActionView();
        tv.setImageResource(R.drawable.ic_cancel);

        item.getActionView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                NavigationMenuItemView i = (NavigationMenuItemView) view.getParent().getParent();
                int itemId = i.getItemData().getItemId();
                Log.v("MainActivity", "DELETE item " + itemId);
                int idx = 0;
                for (int a = 0; a < locations.size(); a++) {
//                for(WeatherLocation locationItem: locations){
                    if (locations.get(a).getUid() == itemId) {
                        db.weatherLocationDao().delete(locations.get(a));
                        idx = a;
                        break;
                    }
                }
                locations.remove(idx);
                menu.removeItem(itemId);

                MenuItem lastItem = menu.getItem(menu.size() - 2);
                lastItem.setChecked(true);
                position = menu.size() - 2;
//                city = locations.get(locations.size() - 2).getCity();
//                country = locations.get(locations.size() - 2).getCountry();
                if(locations.size() > 0) {
                    update(locations.get(locations.size() - 1).getUid());
                }
                return false;
            }
        });
    }

    private void update(int id) {
//        wc = new WeatherController(this, this.city, this.country);
        if(locations.size() > 0) {
            for(WeatherLocation location: locations){
                if (location.getUid() == id){
                    ImageView loadingAnimation = findViewById(R.id.loading_image);
                    loadingAnimation.setVisibility(View.VISIBLE);
                    loadingAnimation.bringToFront();
                    ((AnimationDrawable) loadingAnimation.getDrawable()).start();
                    wc = new WeatherController(this,
                            location.getCity(),
                            location.getCountry());
                    wc.execute();
                    break;
                }
            }
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Log.v("MainActivity", "cancel location dialog");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(locations.size() > 0) {
            update(locations.get(0).getUid());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // build menu
        this.locations = new ArrayList<>();
        this.locations.addAll(db.weatherLocationDao().getAll());
        for (WeatherLocation location : this.locations) {
            Log.v("MainActivity", "" + location.getUid());
            if (this.menu.findItem(location.getUid()) == null) {
                this.addMenuItem(location);
            }
        }
//        MenuItem lastItem = menu.getItem(0).setChecked(true);
    }

}

