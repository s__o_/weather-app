package com.example.simon.weather;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Simon on 20.06.18.
 */

public class WeatherController extends AsyncTask {

    private String city;
    private String country;
    public AsyncResponse delegate = null;
    private JSONObject currentJSON;
    private JSONObject forecastJSON;

    public WeatherController(AsyncResponse delegate, String city, String country){
        this.delegate = delegate;
        this.city = city;
        this.country = country;
    }

    @Override
    protected Object doInBackground(Object[] objects) {


        HttpURLConnection connection = null;
        String targetURL = "http://api.openweathermap.org/data/2.5/weather?q="+this.city+"," +
                ""+this.country+"&APPID=&units=metric";

        String forecastTargetURL = "http://api.openweathermap.org/data/2.5/forecast?q="+this.city+"," +
                ""+this.country+"&APPID=&units=metric";
        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

//            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream());
//            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            currentJSON = new JSONObject(response.toString());



            // ########
            url = new URL(forecastTargetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            wr = new DataOutputStream (
                    connection.getOutputStream());
//            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response
            is = connection.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));
            response = new StringBuilder(); // or StringBuffer if Java version 5+
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            forecastJSON = new JSONObject(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }

            return null;
        }
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        delegate.processFinish(currentJSON, forecastJSON);
    }

    public interface AsyncResponse {
        void processFinish(JSONObject current, JSONObject forecast);
    }

    public int dayProgress(Date sunrise, Date sunset){
        Date currTime = new Date(System.currentTimeMillis());
        long total = sunset.getTime() - sunrise.getTime();
        long curr = currTime.getTime() - sunrise.getTime();

        if(currTime.getTime() <= sunset.getTime()){
            // day time
            double progress = ((double) curr / total) * 100;
            return (int) progress;
        }else{
            // at night
            return 0;
        }
    }

    public int getWeatherDesc(int weatherCode) {
        if(weatherCode > 801){
            // clouds
            return R.string.clouds;
        }else if(weatherCode == 801){
            // few clouds
            return R.string.few_clouds;
        }else if(weatherCode == 800){
            // clear
            return R.string.clear;
        }else if(weatherCode >= 701){
            // fog
            return R.string.fog;
        }else if(weatherCode >= 600){
            // snow
            return R.string.snow;
        }else if(weatherCode >= 501) {
            // rain
            return R.string.rain;
        }else if(weatherCode == 500){
            return R.string.light_rain;
        }else if(weatherCode >= 300){
            // drizzle
            return R.string.drizzle;
        }else if(weatherCode >= 200){
            // thunderstorm
            return R.string.thunderstorm;
        }else {
            return -1;
        }
    }

    public int getWeatherIcon(int weatherCode) {
        int iconId = 0;
        if(weatherCode > 801){
            // clouds
            iconId = R.drawable.ic_cloud;
        }else if(weatherCode == 801){
            // few clouds
            iconId = R.drawable.ic_few_clouds;
        }else if(weatherCode == 800){
            // clear
            iconId = R.drawable.ic_sun;
        }else if(weatherCode >= 701){
            // fog
            iconId = R.drawable.ic_fog;
        }else if(weatherCode >= 600){
            // snow
            iconId = R.drawable.ic_snow;
        }else if(weatherCode >= 500){
            // rain
            iconId = R.drawable.ic_rain;
        }else if(weatherCode >= 300){
            // drizzle
            iconId = R.drawable.ic_rain;
        }else if(weatherCode >= 200){
            // thunderstorm
            iconId = R.drawable.ic_thunder;
        }

        return iconId;
    }

    public CombinedData drawChart(Activity activity, JSONArray rawData, CombinedChart chart){

        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<Entry> lineEntries = new ArrayList<Entry>();
        ArrayList<String> labels = new ArrayList<String>();

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH");


        for (int i = 0; i < rawData.length(); i++){
            try {
                JSONObject a = new JSONObject(rawData.get(i).toString());
                JSONObject mainData = new JSONObject(a.get("main").toString());
                lineEntries.add(new Entry(i, (int) mainData.getDouble("temp")));

                double rain = 0.0;
                try{
                    JSONObject rainData = new JSONObject(a.get("rain").toString());
                    rain = rainData.getDouble("3h");
                }catch(JSONException e){

                }


                Date time = new Date(a.getLong("dt") * 1000);
//                Log.v("MainActivity", timeFormat.format(time).toString());
                barEntries.add(new BarEntry(i, (float) rain));
                labels.add(timeFormat.format(time));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(i == 8){
                break;
            }
        }
        LineDataSet lineDataSet = new LineDataSet(lineEntries, null);
        lineDataSet.setColor(ContextCompat.getColor(activity, R.color.black));
        lineDataSet.setCircleColor(ContextCompat.getColor(activity, R.color.black));
        lineDataSet.setCircleColorHole(ContextCompat.getColor(activity, R.color.black));
        lineDataSet.setCircleRadius((float) 4.0);
        lineDataSet.setLineWidth((float) 3.0);
        LineData lineData = new LineData(lineDataSet);



        BarDataSet barDataset = new BarDataSet(barEntries, null);
        barDataset.setColor(ContextCompat.getColor(activity, R.color.colorAccent));
        BarData barData = new BarData(barDataset);




        CombinedData data = new CombinedData();
        data.setData(barData);
        data.setData(lineData);




        // customize x labels
        XAxis xAxis = chart.getXAxis();
        String[] values = labels.toArray(new String[labels.size()]);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

        // some ui changes
        chart.getLegend().setEnabled(false);
        chart.getAxisLeft().setDrawLabels(false);
        chart.getAxisRight().setDrawLabels(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getXAxis().setDrawAxisLine(false);
        chart.setDescription(null);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setTouchEnabled(false);


        return data;
    }
}
