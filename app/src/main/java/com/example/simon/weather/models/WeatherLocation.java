package com.example.simon.weather.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Simon on 03.07.18.
 */
@Entity
public class WeatherLocation {

    public static int COUNT = 0;

    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "city")
    private String city;

    @ColumnInfo(name = "country")
    private String country;

    public WeatherLocation(int uid, String city, String country){
        this.uid = uid;
        this.city = city;
        this.country = country;
    }

    @Ignore
    public WeatherLocation(String city, String country){
        COUNT += 1;
        this.uid = COUNT;
        this.city = city;
        this.country = country;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
